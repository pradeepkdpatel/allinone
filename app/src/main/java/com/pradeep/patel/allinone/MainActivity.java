package com.pradeep.patel.allinone;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("In onCreate");
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("In onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("In onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("In onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("In onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("In onDestroy");
    }

    /**
     *  Other Callback methods
     */

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        System.out.println("In onPostCreate");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("In onRestart");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        System.out.println("In onRestoreInstanceState");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        System.out.println("In onPostResume");
    }

}
