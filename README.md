Adapter View - To display dynamic content
             - Can display millions of items very efficiently
             - E.g., List View, Grid View and Spinner.
             - Common data sets are arrays, lists and cursor objects
             - common adapters are ArrayAdapter and SimpleCursorAdapter